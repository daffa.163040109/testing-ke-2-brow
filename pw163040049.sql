-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 18, 2018 at 04:41 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pw163040049`
--

-- --------------------------------------------------------

--
-- Table structure for table `tokoh`
--

CREATE TABLE `tokoh` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `caption` varchar(25) NOT NULL,
  `born` varchar(50) NOT NULL,
  `details` varchar(300) NOT NULL,
  `link` varchar(50) NOT NULL,
  `image` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tokoh`
--

INSERT INTO `tokoh` (`id`, `name`, `caption`, `born`, `details`, `link`, `image`) VALUES
(34, '', '', '', '', '', '5b0ff05e0a6df.jpg'),
(35, '', '', '', '', '', '5b0ff075c9ac4.png'),
(39, 'rgd', 'ewfsd', 'sfd', 'sfd', 'sdfg', '5b5817f804a51.jpg'),
(40, 'jhzkjd', 'kjbskdjbkj', 'kjbsdkjbk', 'jbkjsdbfkjbk``', 'kjsbdkjs', '5ba33f2f4a54d.jpg'),
(43, 'yana', 'nugraha', '1997', '', '', '5bc7f0ece8d5b.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `full_name` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `username` varchar(7) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `full_name`, `email`, `username`, `password`, `level`) VALUES
(17, 'qwert', 'qwerty', 'qwerty', '$2y$10$XRn/LcaJJGIVi.BSRW7QtekUH0pYUcXdW.dMjRYylgaQGfAcNENLS', 1),
(18, 'ew', 'ew', 'ew', '$2y$10$5VRwXxU.ZmBpgQLa7.wXXOEp.4lBPMsTq/xd3yekg8S5RAEGO0/rK', 0),
(19, 're', 're', 're', '$2y$10$cFybbuP1BRS8TzVzn0z/oeFjbFJHisqycgitTNGCjDcH7crzk6sBa', 0),
(20, 'andi', 'asdkn', 'q', '$2y$10$vDTU0qH.MDEMKeD9h/6m9.KHCtCM/G5kMAm7Hxk0WXcce.E4gWfpi', 0),
(21, 'tes', 'tes', 'tes', '$2y$10$3bAvpsz43/MYldChivWNy.b.en7rTaLXDKO7eN7IN1A42QnElJcf6', 0),
(22, 'd', 'd', 'd', '$2y$10$n1AqJhRb3ryvTseYDI6FGeDM/pdqzDTT/no9S6YIvZTiznx3iCRXi', 0),
(23, 'po', 'po', 'po', '$2y$10$1UrPimKIpvVOrxV2EhhIFeybsY7rR67LMT8kykAzmFWXHho0AJu5G', 0),
(24, 'u', 'u', 'u', '$2y$10$F2QFqP4FwdWX3PdELrM3O.wMHQdtL1l6qlj93XIaJFfQ1dR/tReqO', 0),
(25, 'Y', 'Y', 'Y', '$2y$10$CN.5lDBFxNq68u9zQ0CjqOVwQA6zQRIEVpzujmElKR0AhZC2WANsa', 0),
(26, 'y', 'y', 'yana', '$2y$10$EB0ZkzUD9fcP7CedyqYAv.bfXMNdZQVYsLJocAtHh38B/03o5ZboS', 0),
(27, 'opik', 'opik', 'opik', '$2y$10$NOvLqJRmRNdAPWeGOaKw5.nAa5OdbtCuG5.lBF1J0J4FzF2UD8OUK', 0),
(28, 'qw', 'qw', 'qw', '$2y$10$sISvhRcC8wtXUht8oxQOP./klmDQMn7..L1QBWIeCUQu5ba08pxl.', 0),
(29, 'danis', 'danis', 'danis', '$2y$10$HA/K38yfn.h/Rv0SIUTl3OenaPmAM7umL6T8vf3vzhuQfm.xpmkwS', 0),
(30, 'Risa', 'risa', 'risa', '$2y$10$bTGvEm2htNlKqKpse6fBBOFwo0SFsKVAEdi0pZXeNdZADv7aXw81O', 0),
(31, 'yey', 'ddjjd', '88', '$2y$10$YNd95qoGbeaGaYt9wlW5NO8JCwzX6nmDLqdReV84TitffT7/SbQZm', 0),
(32, 'yana2', 'y', 'yy2', '$2y$10$y/HhxQlyo1p3Oq3MMnXWv.4H1GPPnAYCEdBLho6yxqR77gE4Uihgm', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tokoh`
--
ALTER TABLE `tokoh`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tokoh`
--
ALTER TABLE `tokoh`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
