
<?php 
	require 'functions.php';
	session_start();
	
	

	//CEK KEBERADAAN SESSION
	if (isset($_SESSION["user"])) {
		$get_user = $_SESSION['user'];
		$val_user = get_username($get_user);
		// var_dump($get_user == $val_user);
		// die;

		if ($get_user == $val_user){
				$val_level = get_level();
				// var_dump($val_level);
				// die;
				if ($val_level == 1) {
					//arahkan ke data-admin.php
					session_start();
					$_SESSION['user']= $username;
					header('Location: data-admin.php');
					die;
				}else{
					//arahkan ke data-user.php
					session_start();
					$_SESSION['user']= $username;
					header('Location: data-user.php');
					die;
				}
		}else {
			header("Location:login.php"); 
			die;
		}
	//CEK KEBERADAAN COOKIE
	}else if (isset($_COOKIE["username"])) {
		$get_cookie = $_COOKIE["username"];
		$val_cookie = get_username($get_cookie);			
		session_start();
		$_SESSION["user"] = $val_cookie;
		header("Location:data-admin.php");
		die;
		if ($val_cookie != $cookie){
			header("Location:login.php");
			die;
		}
	//CEK KEBERADAAN SESSION
	} else{
		if(isset($_POST["login"])) {
			$conn = koneksi();
			$username = $_POST['username'];
			$result = mysqli_query($conn, "SELECT * FROM  user WHERE username= '$username' ");
			if (mysqli_num_rows($result) === 1) {
				
				$row = mysqli_fetch_assoc($result);
				$password = $_POST['password'];
				if ( password_verify($password, $row['password']) ) {
					if (isset($_POST["remember"])) {
						setcookie("username", $_POST["username"], time()+60*60*60*24);
					}
					$val_level = get_level();
					if ($val_level == 1) {
						//arahkan ke data-admin.php
						session_start();
						$_SESSION['user']= $username;
						header('Location: data-admin.php');
						die;
					}else{
						//arahkan ke data-user.php
						session_start();
						$_SESSION['user']= $username;
						header('Location: data-user.php');
						die;
					}
				}
			}else {
				$error = true;
			}
		}
	}
?>

<!--*************************************************-->




<!-- HTML -->

<?php require 'head.php'; ?>
	

	<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>> NAVBAR-->

	<nav class="navbar navbar-custom navbar-fixed-top">
        <div class="container">
            <a class="back-button" href="index.php">
                <div class="nav-button"><img src="assets/img/back-button.svg"></div>
            </a>
        </div>
    </nav>

	
	<!-- >>>>>>>>>>>>>>>>>>>>>>>>> HEADER-->
	<div class="header">
		
			<h2 class="text-center">Login</h2>

			<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>> FORM -->
			<form method="post" action="" class="log-input">
				<input type="text" class="form-control"  name="username" placeholder="username" autofocus> 
				<input type="password" class="form-control"  name="password" placeholder="password">
				<div class="checkbox">
					<label><input type="checkbox" name="remember">Remember me</label>
				</div>

				<button type="submit" class="btn btn-default form-control" name="login">Submit</button>
			</form>

			<!--*****************************************-->

				<!-- pengecekan jika data yg diinputkan salah -->
				<?php if (isset($error)) : ?> 
					<p class="mt50" style="color: red; ">username salah</p>
				<?php endif; ?>
				<?php if (isset($error_password)) : ?> 
					<p class="mt50" style="color: red; ">password salah</p>
				<?php endif; ?>

			<!--*****************************************-->
	</div><!-- HEADER -->

	
</body>
</html>