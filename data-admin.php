<!--
*************************************
MENAMPILKAN DATA DARI DB
************************************* -->



<?php 
	require "functions.php";
	// if tombol "search" ditekan
	if (isset($_GET['search'])) {
		// get keyword dari inputan
		$keyword = $_GET['keyword'];
		// --
		$tokoh = "SELECT * FROM tokoh
					WHERE name LIKE '%$keyword%' OR
					caption LIKE '%$keyword%'
					ORDER BY name
				";
		// query data sesuai keyword
		$tokoh = query($tokoh);
	} else {
		// tampilkan semua data
		$tokoh = query("SELECT * FROM tokoh ORDER BY id DESC");

	}
?>

<!--
*************************************
CEK KEBERADAAN SESSION
************************************* -->

<?php 
	session_start();
	$get_user = $_SESSION['user'];
	$val_user = get_username($get_user);

	// cek apakah ada user?
	if (!isset($_SESSION["user"])) {
		header("Location:login.php");
		die;
	}
	else {
		// jika username tdk sama dgn di database
		if ($_SESSION["user"] != $val_user) {
			header("Location: login.php");
			die;
		}
	}
?>




<!-- HTML  -->
	
	<!-- head of page -->
	<?php require 'head.php'; ?>

	<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> NAVBAR -->

	<nav class="navbar navbar-custom navbar-fixed-top">
	    <div class="container">
	            <a class="back-button" href="index.php">
	                <div class="nav-button"><img src="assets/img/back-button.svg"></div>
	            </a>
	            <a class="next-button" href="logout.php">
	                <div class="nav-button"><img src="assets/img/next-button.svg"></div>
	            </a>
	            <a class="next-button" href="insert-data.php">
	                <div class="nav-button"><img src="assets/img/insert.svg"></div>
	            </a>      
	    </div>
	</nav>

	<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> SMALL HERO -->

	<div class="content text-center">
		<div class="col4">
			<h1>national hero<br> of indonesia</h1>
			<form action="" method="get">
				<input type="text"  name="keyword" class="form-control" placeholder="search" id="keyword" autofocus>
				<button class="btn btn-search" name="search" id="search" type="submit">submit</button>
			</form>
		</div>
	</div>
		

	<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CONTENT-->
 	<div id="wrap">
		<div class="container">
			<div  class="content-galery-admin ">

				<!-- ********************************** PHP DATA TIDAK ADA -->
				<?php if(empty($tokoh)) : ?>
					<div style="text-align: center">
						<h3>Data not found</h3>
					</div>
				<?php endif; ?>

			
				<?php  foreach ($tokoh as $row): ?>
					<div class="col-sm-3">

						<a href="detail-admin.php?
								id=<?=$row["id"]?>
								&image=<?=$row["image"]?>
								&name=<?=$row["name"]?>
								&caption=<?=$row["caption"]?>
								&born=<?=$row["born"]?>
								&details=<?=$row["details"]?>
								&link=<?=$row["link"]?>
								">

							<div class="thumbnail">				
							<img src="assets/img/<?=$row["image"]?>" alt="">
							<div class="caption">
								<h4><?=$row["name"]?></h4>
								<p><?=$row["caption"]?></p>	
							</div>	
							<div class="content-button">
								<a href="update-data.php?id=<?= $row["id"]; ?>"> <div class="button-admin"><img src="assets/img/edit.svg" ></a></div>
								<a href="delete-data.php?id=<?= $row["id"]; ?>" onclick="return confirm('yakin?')";><div class="button-admin"><img src="assets/img/delete.svg" ></a></div>
							</div>			
						</div></a>
					</div>
				<?php endforeach; ?>
			</div><!--container-galery-->
		</div><!--container-->
	</div>

	<div class="footer"></div>

	<!-- jika ajax menggunaka jquery -->
	<!-- <script src="plugin/jquery/jquery-3.1.1.min.js"></script> -->
	<script src="assets/js/searchAjax.js"></script>
</body>
</html>