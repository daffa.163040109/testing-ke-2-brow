<?php 
/*******************************************
KONEKSI KE DATABASE
********************************************/
function koneksi(){
	$conn = mysqli_connect("localhost", "root", "", "pw163040049")  or die( "koneksi gagal");
	mysqli_select_db($conn, "pw163040049" ) or die("database salah!");
	return $conn;
} 
 

/*******************************************
QUERY DATA
********************************************/
function query($sql) {
 	$conn = koneksi();
 	$result = mysqli_query($conn, $sql);
 	$rows = [];

 	while($row = mysqli_fetch_assoc($result)){
 		$rows[] = $row;
 	}
 	return $rows;
}

/*******************************************
FUNGSI HAPUS
********************************************/

function hapus($id){
	$conn = koneksi();
	mysqli_query($conn, "DELETE FROM tokoh WHERE id=$id");

	return mysqli_affected_rows($conn);
}


/*******************************************
FUNGSI TAMBAH
********************************************/

function tambah($data) {
	$conn = koneksi();

	$name = $data["name"];
	$caption = $data["caption"];
	$born = $data["born"];
	$details = $data["details"];
	$link = $data["link"];

	//menaganni file gambar
	$nama_file = $_FILES['image']['name'];
	$tipe_file = $_FILES["image"]['type'];
	$ukuran_file = $_FILES["image"]['size'];
	$error_file = $_FILES["image"]['error'];
	$tmp_file = $_FILES["image"]['tmp_name'];


	//pindahkan file ditemporari ke forder img
	// kemudian pindahhi namanya ke database
	// print_r($_FILES['gambar']);
	// die;

	//upload gambar

	//cek apakah ada file yg diupload/tidak
	if ($error_file === 4) {
		echo "<script>
				alert('pilih gambar terlebih dahulu');
				document.location.href= 'insert-data.php';
			</script>";
		return false;
	}

	// cek apakah gambar/bukan
	$gambar_aman = ['jpg','jpeg','png', 'gif'];
	$extensi_file = explode('.', $nama_file);
	$extensi_file = strtolower(end($extensi_file));

	if (! in_array($extensi_file, $gambar_aman)) {
		echo "<script>
				alert('harus type gambar');
				document.location.href= 'insert-data.php';
			</script>";
		return false;
	}

	if ( $ukuran_file > 2000000) {
		echo "<script>
				alert('ukuran file terlalu besar');
				document.location.href= 'insert-data.php';
			</script>";
		return false;
	}

	//mengubah nama file gambar

	$nama_file_baru = uniqid(). '.' .$extensi_file;

	move_uploaded_file($tmp_file, 'assets/img/' . $nama_file_baru);
	$image = $nama_file_baru;
	// var_dump($image);
	// die;






	$sql = "INSERT INTO 
			tokoh
			VALUES ('','$name','$caption','$born','$details','$link','$image');
			";

	mysqli_query($conn, $sql);

	return mysqli_affected_rows($conn);
}



/*******************************************
FUNGSI UBAH
********************************************/

function ubah($data){
		$conn = koneksi();

		$id = $data["id"];
		$name = htmlspecialchars($data["name"]);
		$caption = htmlspecialchars($data["caption"]);
		$born = htmlspecialchars($data["born"]);
		$details = htmlspecialchars($data["details"]);
		$link = htmlspecialchars($data["link"]);
		$image = htmlspecialchars($data["gambar_lama"]);


		// cek apakah user upload gambar baru

		if ($_FILES['image']['error'] == 0) {
			$nama_file = $_FILES['image']['name'];
			$ukuran_file = $_FILES["image"]['size'];
			$tmp_file = $_FILES["image"]['tmp_name'];

			// cek apakah gambar/bukan
			$gambar_aman = ['jpg','jpeg','png', 'gif'];
			$extensi_file = explode('.', $nama_file);
			$extensi_file = strtolower(end($extensi_file));

			if (! in_array($extensi_file, $gambar_aman)) {
				echo "<script>
						alert('harus type gambar');
						document.location.href= '';
					</script>";
				return false;
			}
				if ( $ukuran_file > 2000000) {
				echo "<script>
						alert('ukuran file terlalu besar');
						document.location.href= '';
					</script>";
				return false;
			}
			//mengubah nama file gambar

			$nama_file_baru = uniqid(). '.' .$extensi_file;

			move_uploaded_file($tmp_file, 'assets/img/' . $nama_file_baru);
			$image = $nama_file_baru;
		}





		$query = "UPDATE tokoh
					SET
					name = '$name',
					caption = '$caption',
					born = '$born',
					details = '$details',
					link = '$link',
					image = '$image'
					WHERE id = '$id'
			";
		mysqli_query($conn, $query);

		return mysqli_affected_rows($conn);
}


function register($data){
	$conn = koneksi();


	$full_name = htmlspecialchars($data["full-name"]);
	$email = htmlspecialchars($data["email"]);
	$username = htmlspecialchars($data["username"]);
	$password = htmlspecialchars($data["password"]);

	// cek keberadaan username di db
	$result = mysqli_query($conn, "SELECT * FROM  user WHERE username= '$username' ");

	// jika ada
	if (mysqli_num_rows($result) === 1) {
		echo "<script>
				alert('username sudah ada!');
				document.location.href='register.php';
			</script>";
		return false;
	}

	//encripsi pass
	$password = password_hash($password, PASSWORD_DEFAULT);

	//insert data user baru
	mysqli_query($conn, "INSERT INTO user VALUES('','$full_name','$email','$username', '$password','')");

	return mysqli_affected_rows($conn);
}

function get_username($query) {
	$conn = koneksi();
	$username_query = mysqli_query($conn, "SELECT username FROM  user WHERE username= '$query' ");
	$get_value = mysqli_fetch_assoc($username_query );
	$val= $get_value["username"];
	return $val;
}

function get_level(){
	$conn = koneksi();
	$username = $_POST['username'];
	$username_query = mysqli_query($conn, "SELECT level FROM  user WHERE username= '$username' ");
	$get_value = mysqli_fetch_assoc($username_query );
	$val = $get_value["level"];
	return $val;
	// var_dump($val);
	// die;
}

// function edit_user($user){
// 	$conn = koneksi();

// 	$get_user = $_SESSION['user'];
// 	$val_user = get_username($get_user);
// 	$user = mysqli_query($conn, "SELECT * FROM  user WHERE username= '$val_user' ");
// 	$user = mysqli_fetch_assoc($user);


// 		$id = $user["user_id"];
// 		$name = htmlspecialchars($user["full_name"]);
// 		$email = htmlspecialchars($user["email"]);
// 		$username = htmlspecialchars($user["username"]);
// 		$password = htmlspecialchars($user["password"]);
// 		$query = "UPDATE user
// 					SET
// 					user_id = '',
// 					full_name = '$name',
// 					email = '$email',
// 					usename = '$username',
// 					password = '$password',
// 					level = ''
// 					WHERE user_id = '$id'
// 			";
// 		$t = mysqli_query($conn, $query);
// 		var_dump($t);
// 		die;

// 		return mysqli_affected_rows($conn);
// }






?>