

<!--******************************** TAMPILKAN DATA DARI DB-->

<?php 
	require "functions.php";
	$tokoh = query("SELECT * FROM tokoh");
?>

<!--
*************************************
CEK KEBERADAAN SESSION
************************************* -->

<?php 
	session_start();
	$get_user = $_SESSION['user'];
	$val_user = get_username($get_user);

	// cek apakah ada user?
	if (!isset($_SESSION["user"])) {
		header("Location:login.php");
		die;
	}
	else {
		// jika username tdk sama dgn di database
		if ($_SESSION["user"] != $val_user) {
			header("Location: login.php");
			die;
		}
	}
?>

<!-- HTML-->

<?php require 'head.php'; ?>
	
	<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> NAVBAR-->

	<nav class="navbar navbar-custom navbar-fixed-top">
        <div class="container">
                <a class="back-button" href="data-user.php">
                    <div class="nav-button"><img src="assets/img/back-button.svg"></div>
                </a>
                <a class="next-button" href="logout.php">
                    <div class="nav-button"><img src="assets/img/next-button.svg"></div>
                </a>         
        </div>
    </nav>


	
	<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CONTENT -->

	<div class="container detile">
		<div class="content-galery-admin ">


			<div class="col-sm-12">
				<div class="thumbnail thumbnail-admin">

					
					
					<img src="assets/img/<?php echo $_GET['image']; ?>" class="main-img">
					
					<div class="caption text-center">
						<h1><?php echo $_GET["name"]; ?></h1>
						<p><?php echo $_GET["caption"]; ?></p>
						<h3><?php echo $_GET["born"]; ?></h3>
					</div>	

					<div class="text-desc">
						<p><?php echo $_GET["details"]; ?></p>
					</div>

					<div class="view-detile text-center">
						<a href="<?php echo $_GET["link"]; ?>" class="btn btn-default">view detile</a>
					</div>			
				</div>
			</div>
		</div>
	</div>

	<div class="footer"></div>
</body>
</html>