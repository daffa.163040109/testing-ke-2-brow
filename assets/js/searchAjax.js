// cari element yang dibutuhkan
var keyword = document.getElementById('keyword');
// var cari = document.getElementById('search');
var wadah = document.getElementById('wrap');

//jalankan fungsi ketika inputan diketik
keyword.addEventListener('keyup', function(){

	//instansiasi objek/ panggil objek ajak
	var xhr = new XMLHttpRequest();

	//cek kesiapan ajax kita 
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 && xhr.status == 200) {
			wadah.innerHTML = xhr.responseText;
		}
	}

	//jalankan ajax
	// xhr.open('get', 'ajax.php?keyword=' + keyword.value, true);
	xhr.open('get', 'ajax.php?keyword=' + keyword.value, true);
	xhr.send();
});


// VERSI JQUERY ************************************** 


// code ini mengandung mekanisme yang keluar dari kebiasaan 
//yangmana pada semestinya
// dari data-admin.php -> searchAjax.js (berada di tempat semestinya) -> ajax.php
// yang artinya searchAjax.js -> ajax.php harus keluar melewati 2 folder(../../)

//tapi ini :
// dari data-admin.php -> searcAjax.js (masuk ke di dalam file data-admin.php) -> ajax.php
// ini artinya data-admin.php yang didalamnya ada code searcAjax -> ajax.php

//????????
//apakah ini hanya berlaku untuk tool ajax aja?
//ini terlihat seperti require pada php


// $('#keyword').on('keyup', function(){
// 	$('#wrap').load('../../ajax.php?keyword=' + $('#keyword').val());
// });