<!--
*************************************
MENAMPILKAN DATA DARI DB
************************************* -->


<!-- code ini berfungsi ketika mengetik "yana" misal, 
kemudian dihapus 3 hurup dari belakang menjadi "y"
maka ia akan mencari dan menampilkan data dengan kata kunci "y"  -->
<?php 
	require "functions.php";
	// get keyword dari inputan
	$keyword = $_GET['keyword'];
	// --
	$tokoh = "SELECT * FROM tokoh
				WHERE name LIKE '%$keyword%' OR
				caption LIKE '%$keyword%'
				ORDER BY id DESC
			";
	// query data sesuai keyword
	$tokoh = query($tokoh);
?>

		




<div class="container">
	<div  class="content-galery-admin">
	
		<!-- ********************************** PHP DATA TIDAK ADA -->
		<?php if(empty($tokoh)) : ?>
			<div style="text-align: center">
				<h3>Data not found</h3>
			</div>
		<?php endif; ?>
	

		<?php  foreach ($tokoh as $row): ?>
			<div class="col-sm-3">
				<a href="detail-admin.php?

						id=<?=$row["id"]?>
						&image=<?=$row["image"]?>
						&name=<?=$row["name"]?>
						&caption=<?=$row["caption"]?>
						&born=<?=$row["born"]?>
						&details=<?=$row["details"]?>
						&link=<?=$row["link"]?>
						">

					<div class="thumbnail">				
					<img src="assets/img/<?=$row["image"]?>" alt="">
					<div class="caption">

						<h4><?=$row["name"]?></h4>
						<p><?=$row["caption"]?></p>	
					</div>	
					<div class="content-button">
						<a href="update-data.php?id=<?= $row["id"]; ?>"> <div class="button-admin"><img src="assets/img/edit.svg" ></a></div>
						<a href="delete-data.php?id=<?= $row["id"]; ?>" onclick="return confirm('yakin?')";><div class="button-admin"><img src="assets/img/delete.svg" ></a></div>
					</div>			
				</div></a>
			</div>
		<?php endforeach; ?>

	</div><!--container-galery-->
</div><!--container-->



