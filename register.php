<?php 
require 'functions.php';

if(isset($_POST["register"])) {
	$password = htmlspecialchars($_POST["password"]);
	$confirm = htmlspecialchars($_POST["confirm"]);
	if ($password != $confirm) {
		$error = true;
	}elseif (register($_POST) > 0) {
		echo "<script>
				alert('data berhasil diTAMBAH');
				document.location.href= 'login.php';
			</script>";
	}else{
		echo "<script>
				alert('data gagal diTAMBAH');
				document.location.href= 'register.php';
			</script>";
	}
}
?>


<?php require 'head.php'; ?>

<!-- *************************************************** -->

<br><br>

<div class="container">
	<div class="row">
		<div class="col-sm-6">
			<h1 class="h-insert">Register</h1>

			<?php if (isset($error)) : ?> 
					<p class="mt50" style="color: red; ">password tidak sesuai</p>
			<?php endif; ?>



			<form class="register" action="" method="post">
	  				<label class="label-form" for="full-name">Nama Lengkap</label>
	  				<div class="name">
	    				<input type="text" class="form-control" name="full-name" placeholder="Yana Nugraha" autofocus>
	    			</div>

	    			<label class="label-form" for="email">Email</label>
	  				<div class="email">
	    				<input type="text" class="form-control" name="email" placeholder="yana@gmail.com">
	    			</div>

	    			<label class="label-form" for="username">Username</label>
	    			<div class="username">
	    				<input type="text" class="form-control" name="username" placeholder="yana9007">
	    			</div>
	    			<label class="label-form" for="password">Password</label>
	    			<div class="password">
	    				<input type="password" class="form-control" name="password" placeholder="********">
	    			</div>
	    			<label class="label-form" for="confirm">Konfirmasi password</label>
	    			<div class="confirm-password">
	    				<input type="password" class="form-control" name="confirm" placeholder="********">
	    			</div>
	    			
			  	<br><br>
	  			<button type="submit" class="btn btn-default" name="register">Ok</button>
	  			
			</form>
			<br><br>

			

		</div>
	</div>
</div>
	
</body>
</html>


<!-- <form action="" method="post">
			<ul style="list-style: none;">
				<li >
					<label for="gambar">gambar</label>
					<input type="text" name="gambar" id="gambar" >
				</li><br>

				<li>
					<label for="nama">nama</label>
					<input type="text" name="nama" id="nama">
				</li><br>
				<li>
					<label for="keterangan">keterangan</label>
					<input type="text" name="keterangan" id="keterangan">
				</li><br>
				<li>
					<label for="lahir">lahir</label>
					<input type="text" name="lahir" id="lahir">
				</li><br>
				<li>
					<label for="detail">detail</label>
					<input type="text" name="detail" id="detail">
				</li><br>
				<li>
					<label for="telusuri">telusuri</label>
					<input type="text" name="telusuri" id="telusuri">
				</li><br>
				<li>
					<button type="submit" name="tambah"> tambah data</button>
				</li>

			</ul>
			</form> -->